package com.android.rssnewsmodernui.Interface_2;

import android.view.View;

public interface ItemClickListener_2 {
    void onClick(View view, int position, boolean isLongClick);
}
