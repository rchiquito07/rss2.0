package com.android.rssnewsmodernui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.rssnewsmodernui.Nacional.elComercio;
import com.android.rssnewsmodernui.Nacional.elExtra;
import com.android.rssnewsmodernui.Nacional.elTelegrafo;
import com.android.rssnewsmodernui.Nacional.elUniverso;
import com.android.rssnewsmodernui.Nacional.laHora;
import com.android.rssnewsmodernui.Nacional.uleam;

import java.nio.channels.InterruptedByTimeoutException;

public class MainActivity extends AppCompatActivity {

    private CardView cardView1, cardView2, cardView3, cardView4, cardView5, cardView6;
    private Animation smalltobig, btta, btta2, btta3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cornerRadius();

        smalltobig = AnimationUtils.loadAnimation(this, R.anim.smalltobig);
        btta = AnimationUtils.loadAnimation(this, R.anim.btta);
        btta2 = AnimationUtils.loadAnimation(this, R.anim.btta2);
        btta3 = AnimationUtils.loadAnimation(this, R.anim.btta3);

        cardView1 = findViewById(R.id.rss_1);
        cardView2 = findViewById(R.id.rss_2);
        cardView3 = findViewById(R.id.rss_3);
        cardView4 = findViewById(R.id.rss_4);
        cardView5 = findViewById(R.id.rss_5);
        cardView6 = findViewById(R.id.rss_6);

        cardView1.startAnimation(btta3);
        cardView2.startAnimation(btta3);
        cardView3.startAnimation(btta3);
        cardView4.startAnimation(btta3);
        cardView5.startAnimation(btta3);
        cardView6.startAnimation(btta3);

        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, elComercio.class);
                startActivity(intent);
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, elUniverso.class);
                startActivity(intent);
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, laHora.class);
                startActivity(intent);
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, elExtra.class);
                startActivity(intent);
            }
        });

        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, elTelegrafo.class);
                startActivity(intent);
            }
        });

        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, uleam.class);
                startActivity(intent);
            }
        });

    }

    public void cornerRadius(){

        Drawable originalDrawable = getResources().getDrawable(R.drawable.elcomercio);
        Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();
        RoundedBitmapDrawable roundedDrawable =
                RoundedBitmapDrawableFactory.create(getResources(), originalBitmap);
        roundedDrawable.setCornerRadius(originalBitmap.getHeight());
        ImageView imageView = (ImageView) findViewById(R.id.imgComercio);
        imageView.setImageDrawable(roundedDrawable);


        Drawable original2Drawable = getResources().getDrawable(R.drawable.universo);
        Bitmap original2Bitmap = ((BitmapDrawable) original2Drawable).getBitmap();
        RoundedBitmapDrawable rounded2Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original2Bitmap);
        rounded2Drawable.setCornerRadius(original2Bitmap.getHeight());
        ImageView imageView2 = (ImageView) findViewById(R.id.imgUniverso);
        imageView2.setImageDrawable(rounded2Drawable);


        Drawable original3Drawable = getResources().getDrawable(R.drawable.hora);
        Bitmap original3Bitmap = ((BitmapDrawable) original3Drawable).getBitmap();
        RoundedBitmapDrawable rounded3Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original3Bitmap);
        rounded3Drawable.setCornerRadius(original3Bitmap.getHeight());
        ImageView imageView3 = (ImageView) findViewById(R.id.imgHora);
        imageView3.setImageDrawable(rounded3Drawable);


        Drawable original4Drawable = getResources().getDrawable(R.drawable.extra);
        Bitmap original4Bitmap = ((BitmapDrawable) original4Drawable).getBitmap();
        RoundedBitmapDrawable rounded4Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original4Bitmap);
        rounded4Drawable.setCornerRadius(original4Bitmap.getHeight());
        ImageView imageView4 = (ImageView) findViewById(R.id.imgExtra);
        imageView4.setImageDrawable(rounded4Drawable);


        Drawable original5Drawable = getResources().getDrawable(R.drawable.telegrafo);
        Bitmap original5Bitmap = ((BitmapDrawable) original5Drawable).getBitmap();
        RoundedBitmapDrawable rounded5Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original5Bitmap);
        rounded5Drawable.setCornerRadius(original5Bitmap.getHeight());
        ImageView imageView5 = (ImageView) findViewById(R.id.imgTelegrafo);
        imageView5.setImageDrawable(rounded5Drawable);


        Drawable original6Drawable = getResources().getDrawable(R.drawable.uleam);
        Bitmap original6Bitmap = ((BitmapDrawable) original6Drawable).getBitmap();
        RoundedBitmapDrawable rounded6Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original6Bitmap);
        rounded6Drawable.setCornerRadius(original6Bitmap.getHeight());
        ImageView imageView6 = (ImageView) findViewById(R.id.imgUleam);
        imageView6.setImageDrawable(rounded6Drawable);
    }

}
