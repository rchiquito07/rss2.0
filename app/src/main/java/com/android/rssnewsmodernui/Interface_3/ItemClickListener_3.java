package com.android.rssnewsmodernui.Interface_3;

import android.view.View;

public interface ItemClickListener_3 {
    void onClick(View view, int position, boolean isLongClick);
}
