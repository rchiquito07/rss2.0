package com.android.rssnewsmodernui.Model_3;

import java.util.List;

public class RSSObject_3 {


    public String status;
    public Feed_3 feed;
    public List<Item_3> items;

    public class Enclosure
    {
        public String link;
    }

        public RSSObject_3(String status, Feed_3 feed, List<Item_3> items) {
            this.status = status;
            this.feed = feed;
            this.items = items;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Feed_3 getFeed() {
            return feed;
        }

        public void setFeed(Feed_3 feed) {
            this.feed = feed;
        }

        public List<Item_3> getItems() {
            return items;
        }

        public void setItems(List<Item_3> items) {
            this.items = items;
        }

}
