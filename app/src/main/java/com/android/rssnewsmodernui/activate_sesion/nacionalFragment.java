package com.android.rssnewsmodernui.activate_sesion;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.rssnewsmodernui.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link nacionalFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link nacionalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class nacionalFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    View vista;
    private CardView cardView1, cardView2, cardView3, cardView4, cardView5, cardView6;
    private Animation smalltobig, btta, btta2, btta3;

    public nacionalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment nacionalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static nacionalFragment newInstance(String param1, String param2) {
        nacionalFragment fragment = new nacionalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista = inflater.inflate(R.layout.fragment_nacional, container, false);

        cornerRadius();

        smalltobig = AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.smalltobig);
        btta = AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.btta);
        btta2 = AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.btta2);
        btta3 = AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.btta3);

        cardView1 = vista.findViewById(R.id.rss_1);
        cardView2 = vista.findViewById(R.id.rss_2);
        cardView3 = vista.findViewById(R.id.rss_3);
        cardView4 = vista.findViewById(R.id.rss_4);
        cardView5 = vista.findViewById(R.id.rss_5);
        cardView6 = vista.findViewById(R.id.rss_6);

        cardView1.startAnimation(btta3);
        cardView2.startAnimation(btta3);
        cardView3.startAnimation(btta3);
        cardView4.startAnimation(btta3);
        cardView5.startAnimation(btta3);
        cardView6.startAnimation(btta3);

        return vista;
    }

    public void cornerRadius(){

        Drawable originalDrawable = getResources().getDrawable(R.drawable.elcomercio);
        Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();
        RoundedBitmapDrawable roundedDrawable =
                RoundedBitmapDrawableFactory.create(getResources(), originalBitmap);
        roundedDrawable.setCornerRadius(originalBitmap.getHeight());
        ImageView imageView = (ImageView) vista.findViewById(R.id.imgComercio);
        imageView.setImageDrawable(roundedDrawable);


        Drawable original2Drawable = getResources().getDrawable(R.drawable.universo);
        Bitmap original2Bitmap = ((BitmapDrawable) original2Drawable).getBitmap();
        RoundedBitmapDrawable rounded2Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original2Bitmap);
        rounded2Drawable.setCornerRadius(original2Bitmap.getHeight());
        ImageView imageView2 = (ImageView) vista.findViewById(R.id.imgUniverso);
        imageView2.setImageDrawable(rounded2Drawable);


        Drawable original3Drawable = getResources().getDrawable(R.drawable.hora);
        Bitmap original3Bitmap = ((BitmapDrawable) original3Drawable).getBitmap();
        RoundedBitmapDrawable rounded3Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original3Bitmap);
        rounded3Drawable.setCornerRadius(original3Bitmap.getHeight());
        ImageView imageView3 = (ImageView) vista.findViewById(R.id.imgHora);
        imageView3.setImageDrawable(rounded3Drawable);


        Drawable original4Drawable = getResources().getDrawable(R.drawable.extra);
        Bitmap original4Bitmap = ((BitmapDrawable) original4Drawable).getBitmap();
        RoundedBitmapDrawable rounded4Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original4Bitmap);
        rounded4Drawable.setCornerRadius(original4Bitmap.getHeight());
        ImageView imageView4 = (ImageView) vista.findViewById(R.id.imgExtra);
        imageView4.setImageDrawable(rounded4Drawable);


        Drawable original5Drawable = getResources().getDrawable(R.drawable.telegrafo);
        Bitmap original5Bitmap = ((BitmapDrawable) original5Drawable).getBitmap();
        RoundedBitmapDrawable rounded5Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original5Bitmap);
        rounded5Drawable.setCornerRadius(original5Bitmap.getHeight());
        ImageView imageView5 = (ImageView) vista.findViewById(R.id.imgTelegrafo);
        imageView5.setImageDrawable(rounded5Drawable);


        Drawable original6Drawable = getResources().getDrawable(R.drawable.uleam);
        Bitmap original6Bitmap = ((BitmapDrawable) original6Drawable).getBitmap();
        RoundedBitmapDrawable rounded6Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original6Bitmap);
        rounded6Drawable.setCornerRadius(original6Bitmap.getHeight());
        ImageView imageView6 = (ImageView) vista.findViewById(R.id.imgUleam);
        imageView6.setImageDrawable(rounded6Drawable);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
