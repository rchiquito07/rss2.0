package com.android.rssnewsmodernui.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class seccionesAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> listaFragment = new ArrayList<>();
    private final List<String> listaTitulo = new ArrayList<>();

    public seccionesAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String titulo){
        listaFragment.add(fragment);
        listaTitulo.add(titulo);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listaTitulo.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return listaFragment.get(position);
    }

    @Override
    public int getCount() {
        return listaFragment.size();
    }
}
