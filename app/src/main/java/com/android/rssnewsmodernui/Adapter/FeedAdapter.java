package com.android.rssnewsmodernui.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.rssnewsmodernui.Interface.ItemClickListener;
import com.android.rssnewsmodernui.Model.RSSObject;
import com.android.rssnewsmodernui.R;
import com.android.rssnewsmodernui.detalles;
import com.squareup.picasso.Picasso;

class FeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
    public TextView txtTitle, txtPubDate, txtContent;
    public CardView container;
    private ItemClickListener itemClickListener;

    public FeedViewHolder(@NonNull View itemView) {
        super(itemView);

        container = (CardView) itemView.findViewById(R.id.container);
        txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        txtPubDate = (TextView) itemView.findViewById(R.id.txtPubDate);
        txtContent = (TextView) itemView.findViewById(R.id.txtContent);

        //SetEvent
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);


    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

        itemClickListener.onClick(v, getAdapterPosition(), false);

    }

    @Override
    public boolean onLongClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), true);
        return false;
    }
}

public class FeedAdapter extends RecyclerView.Adapter<FeedViewHolder>{
    private RSSObject rssObject;
    private Context mContext;
    private LayoutInflater inflater;

    public FeedAdapter(RSSObject rssObject, Context mContext) {
        this.rssObject = rssObject;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.row,parent, false);
        return new FeedViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull final FeedViewHolder holder, int position) {

        holder.container.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtTitle.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtPubDate.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtContent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));

        holder.txtTitle.setText(rssObject.getItems().get(position).getTitle());
        holder.txtPubDate.setText(rssObject.getItems().get(position).getPubDate());
        holder.txtContent.setText(rssObject.getItems().get(position).getDescription());


        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(!isLongClick)
                {
                    Intent browserIntent = new Intent(mContext, detalles.class);
                    browserIntent.putExtra("enlace",rssObject.getItems().get(position).getLink());
                    mContext.startActivity(browserIntent);

                }

            }
        });
    }

    /* private void seleccionar(){
        final CharSequence[] opciones={"Abrir en navegador","Añadir a favoritos","Cancelar"};
        final AlertDialog.Builder alertOpciones = new AlertDialog.Builder(mContext);
        alertOpciones.setTitle("Selecione");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (opciones[which].equals("Abrir en navegador")) {
                    Toast.makeText(mContext, "Abrir", Toast.LENGTH_SHORT).show();
                } else {
                    if (opciones[which].equals("Añadir a favoritos")) {
                        Toast.makeText(mContext, "Añadir a favoritos", Toast.LENGTH_SHORT).show();
                    } else {
                        dialog.dismiss();
                    }
                }
            }
        });
        alertOpciones.show();
    }*/

    @Override
    public int getItemCount() {
        return rssObject.items.size();
    }
}

