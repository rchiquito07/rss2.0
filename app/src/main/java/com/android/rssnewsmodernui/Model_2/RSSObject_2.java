package com.android.rssnewsmodernui.Model_2;

import java.util.List;

public class RSSObject_2 {


    public String status;
    public Feed_2 feed;
    public List<Item_2> items;

    public class Enclosure
    {
        public String link;
    }

        public RSSObject_2(String status, Feed_2 feed, List<Item_2> items) {
            this.status = status;
            this.feed = feed;
            this.items = items;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Feed_2 getFeed() {
            return feed;
        }

        public void setFeed(Feed_2 feed) {
            this.feed = feed;
        }

        public List<Item_2> getItems() {
            return items;
        }

        public void setItems(List<Item_2> items) {
            this.items = items;
        }

}
