package com.android.rssnewsmodernui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class sign_in extends AppCompatActivity {

    private ImageView imageView;
    private Animation smalltobig, btta, btta2;
    private TextView textView, textView2, textView3, subtitle_header;
    private Button button, button2;
    private EditText editText, editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        // load this animation
        smalltobig = AnimationUtils.loadAnimation(this, R.anim.smalltobig);
        btta = AnimationUtils.loadAnimation(this, R.anim.btta);
        btta2 = AnimationUtils.loadAnimation(this, R.anim.btta2);

        imageView = findViewById(R.id.imageView);

        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);
        subtitle_header = findViewById(R.id.subtitle_header);

        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);

        // passing animation and start it
        imageView.startAnimation(smalltobig);

        textView.startAnimation(btta);
        textView2.startAnimation(btta2);
        textView3.startAnimation(btta2);
        subtitle_header.startAnimation(btta);

        button.startAnimation(btta2);
        button2.startAnimation(btta2);

        editText.startAnimation(btta2);
        editText2.startAnimation(btta2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(sign_in.this, loginActivity.class);
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(sign_in.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
