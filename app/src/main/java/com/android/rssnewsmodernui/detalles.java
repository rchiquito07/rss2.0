package com.android.rssnewsmodernui;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class detalles extends AppCompatActivity {

    WebView webView;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Rss News");
        setSupportActionBar(toolbar);

        webView();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_detalles, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            webView();
            Snackbar.make(webView, "Cargando", Snackbar.LENGTH_LONG).show();
            return true;
        }
        else if (item.getItemId() == R.id.menu_share){
            try {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plan");
                i.putExtra(Intent.EXTRA_SUBJECT, "RSS News, informandote");
                String body = webView.getTitle() + "\n" + "\n" + webView.getUrl() + "\n" + "\n" + " Compartir por RSS Movil" ;
                i.putExtra(Intent.EXTRA_TEXT, body);
                i.setType("text/plain");
                startActivity(Intent.createChooser(i, "Compartir por:"));
            }catch (Exception e){
                Toast.makeText(this, "No se pudo compartir", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void webView()  {

        webView = (WebView) findViewById(R.id.webView);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setLoadsImagesAutomatically(true);

        Bundle bundle = getIntent().getExtras();
        String url = bundle.getString("enlace");
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient());


        webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url){

                return false;
            }
        });
    }


}
