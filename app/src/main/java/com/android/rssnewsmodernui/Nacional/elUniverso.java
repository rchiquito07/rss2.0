package com.android.rssnewsmodernui.Nacional;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.rssnewsmodernui.Adapter.FeedAdapter;
import com.android.rssnewsmodernui.Adapter_3.FeedAdapter_3;
import com.android.rssnewsmodernui.Common.HTTPDataHandler;
import com.android.rssnewsmodernui.MainActivity;
import com.android.rssnewsmodernui.Model.RSSObject;
import com.android.rssnewsmodernui.Model_3.RSSObject_3;
import com.android.rssnewsmodernui.R;
import com.google.gson.Gson;

public class elUniverso extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    Toolbar toolbar;
    RecyclerView recyclerView;
    RSSObject_3 rssObject_3;
    private SwipeRefreshLayout swipeRefreshLayout;

    private final String RSS_link = "http://www.eluniverso.com/rss/portada.xml";

    private final String RSS_to_Json_API = " https://api.rss2json.com/v1/api.json?rss_url=";

    public static  final  String apiKey = "&api_key=at0omtysfd0fopsbkzur7jdlaqop0kstc2le8xlm";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_el_universo);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.white_blue);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("El Universo");
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        checkInternet();

    }

    private void checkInternet(){

        final ProgressDialog mDialog = new ProgressDialog(elUniverso.this);

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()){
            onLoadingRefresh();
        } else {
            mDialog.setTitle("RSS");
            mDialog.setMessage("Por favor espere ...");
            mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mDialog.setCancelable(false);
            mDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(getBaseContext(),MainActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                    mDialog.dismiss();
                }
            });
            mDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Ajustes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));

                }
            });
            mDialog.show();
            Toast.makeText(getApplicationContext(), "Ups... Algo salio mal, comprueba tu conexion e intenta otra vez", Toast.LENGTH_LONG).show();
        }
    }

    private void loadRSS() {
        swipeRefreshLayout.setRefreshing(true);
        AsyncTask<String, String, String> loadRSSAsync = new AsyncTask<String, String, String>() {


            ProgressDialog mDialog = new ProgressDialog(elUniverso.this);

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setRefreshing(true);
                mDialog.setTitle("RSS");
                mDialog.setMessage("Por favor espere ...");
                mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mDialog.setCancelable(false);
                mDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getBaseContext(),MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                String result;
                HTTPDataHandler http = new HTTPDataHandler();
                result = http.GetHTTPData(params[0]);
                return result;
            }


            @Override
            protected void onPostExecute(String s) {
                swipeRefreshLayout.setRefreshing(false);
                mDialog.dismiss();
                rssObject_3 = new Gson().fromJson(s, RSSObject_3.class);
                FeedAdapter_3 adapter = new FeedAdapter_3(rssObject_3, getBaseContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        };

        StringBuilder url_get_data = new StringBuilder(RSS_to_Json_API);
        url_get_data.append(RSS_link + apiKey);
        loadRSSAsync.execute(url_get_data.toString());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh)

            swipeRefreshLayout.setRefreshing(true);
        onLoadingRefresh();

        return true;
    }

    @Override
    public void onRefresh() {
        loadRSS();
    }
    private void onLoadingRefresh(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    loadRSS();
                }
            });
        } else {
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getApplicationContext(), "Ups... Algo salio mal, comprueba tu conexion e intenta otra vez", Toast.LENGTH_LONG).show();
        }
    }
}
