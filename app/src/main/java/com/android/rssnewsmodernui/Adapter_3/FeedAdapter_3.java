package com.android.rssnewsmodernui.Adapter_3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.rssnewsmodernui.Interface_3.ItemClickListener_3;
import com.android.rssnewsmodernui.Model_3.RSSObject_3;
import com.android.rssnewsmodernui.R;
import com.android.rssnewsmodernui.detalles;
import com.squareup.picasso.Picasso;

class FeedViewHolder_3 extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
    public TextView txtTitle, txtPubDate, txtContent;
    public CardView container;
    public ImageView imgView;
    private ItemClickListener_3 itemClickListener;

    public FeedViewHolder_3(@NonNull View itemView) {
        super(itemView);

        container = (CardView) itemView.findViewById(R.id.container);
        txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        txtPubDate = (TextView) itemView.findViewById(R.id.txtPubDate);
        txtContent = (TextView) itemView.findViewById(R.id.txtContent);
        imgView = (ImageView) itemView.findViewById(R.id.imgView);

        //SetEvent
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);


    }

    public void setItemClickListener(ItemClickListener_3 itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

        itemClickListener.onClick(v, getAdapterPosition(), false);

    }

    @Override
    public boolean onLongClick(View v) {

        itemClickListener.onClick(v, getAdapterPosition(), true);
        return true;
    }
}

public class FeedAdapter_3 extends RecyclerView.Adapter<FeedViewHolder_3>{
    private RSSObject_3 rssObject;
    private Context mContext;
    private LayoutInflater inflater;

    public FeedAdapter_3(RSSObject_3 rssObject, Context mContext) {
        this.rssObject = rssObject;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public FeedViewHolder_3 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.row_3,parent, false);
        return new FeedViewHolder_3(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder_3 holder, int position) {

        holder.container.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtTitle.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtPubDate.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtContent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.imgView.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));

        holder.txtTitle.setText(rssObject.getItems().get(position).getTitle());
        holder.txtPubDate.setText(rssObject.getItems().get(position).getPubDate());
        holder.txtContent.setText(rssObject.getItems().get(position).getDescription());

        Picasso.with(mContext).load(rssObject.getItems().get(position).getEnclosure().link).into(holder.imgView);

        holder.setItemClickListener(new ItemClickListener_3() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if( !isLongClick)
                {
                    Intent browserIntent = new Intent(mContext, detalles.class);
                    browserIntent.putExtra("enlace",rssObject.getItems().get(position).getLink());
                    mContext.startActivity(browserIntent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return rssObject.items.size();
    }
}

