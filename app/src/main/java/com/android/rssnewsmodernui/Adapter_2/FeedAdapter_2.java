package com.android.rssnewsmodernui.Adapter_2;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.android.rssnewsmodernui.Interface_2.ItemClickListener_2;
import com.android.rssnewsmodernui.Model_2.RSSObject_2;
import com.android.rssnewsmodernui.Model_3.RSSObject_3;
import com.android.rssnewsmodernui.R;
import com.android.rssnewsmodernui.detalles;

class FeedViewHolder_2 extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
    public TextView txtTitle, txtPubDate, txtContent;
    public CardView container;
    private ItemClickListener_2 itemClickListener;

    public FeedViewHolder_2(@NonNull View itemView) {
        super(itemView);

        container = (CardView) itemView.findViewById(R.id.container);
        txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        txtPubDate = (TextView) itemView.findViewById(R.id.txtPubDate);
        txtContent = (TextView) itemView.findViewById(R.id.txtContent);

        //SetEvent
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);


    }

    public void setItemClickListener(ItemClickListener_2 itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

        itemClickListener.onClick(v, getAdapterPosition(), false);

    }

    @Override
    public boolean onLongClick(View v) {

        itemClickListener.onClick(v, getAdapterPosition(), true);
        return true;
    }
}

public class FeedAdapter_2 extends RecyclerView.Adapter<FeedViewHolder_2>{
    private RSSObject_2 rssObject;
    private Context mContext;
    private LayoutInflater inflater;

    public FeedAdapter_2(RSSObject_2 rssObject, Context mContext) {
        this.rssObject = rssObject;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public FeedViewHolder_2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.row_2,parent, false);
        return new FeedViewHolder_2(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder_2 holder, int position) {

        holder.container.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtTitle.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtPubDate.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));
        holder.txtContent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_animation));

        holder.txtTitle.setText(rssObject.getItems().get(position).getTitle());
        holder.txtPubDate.setText(rssObject.getItems().get(position).getPubDate());
        holder.txtContent.setText(rssObject.getItems().get(position).getDescription());

        holder.setItemClickListener(new ItemClickListener_2() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if( !isLongClick)
                {
                    Intent browserIntent = new Intent(mContext, detalles.class);
                    browserIntent.putExtra("enlace",rssObject.getItems().get(position).getLink());
                    mContext.startActivity(browserIntent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return rssObject.items.size();
    }
}

